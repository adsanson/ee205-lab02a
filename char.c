///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205 - Object Oriented Programming
/// Lab 02a - Datatypes
///
/// @file char.c
/// @version 1.0
///
/// Print the characteristics of the "char", "signed char" and "unsigned char" datatypes.
///
/// @author Albert D'Sanson <adsanson@hawaii.edu>
/// @brief  Lab 02 - Datatypes - EE 205 - Spr 2021
/// @date   25_01_2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <limits.h>

#include "datatypes.h"
#include "char.h"


///////////////////////////////////////////////////////////////////////////////
/// char

/// Print the characteristics of the "char" datatype
void doChar() {
   printf(TABLE_FORMAT_CHAR, "char", CHAR_BIT, CHAR_BIT/8, CHAR_MIN, CHAR_MAX);
}


/// Print the overflow/underflow characteristics of the "char" datatype
void flowChar() {
   char overflow = CHAR_MAX;
   printf("char overflow:  %d + 1 ", overflow++);
   printf("becomes %d\n", overflow);

   char underflow = CHAR_MIN;
   printf("char underflow:  %d - 1 ", underflow--);
   printf("becomes %d\n", underflow);
}


///////////////////////////////////////////////////////////////////////////////
/// signed char

/// Print the characteristics of the "signed char" datatype
void doSignedChar() {
   printf(TABLE_FORMAT_CHAR, "signed char", CHAR_BIT, CHAR_BIT/8, SCHAR_MIN, SCHAR_MAX);
}


/// Print the overflow/underflow characteristics of the "signed char" datatype
void flowSignedChar() {
   signed char overflow = SCHAR_MAX;
   printf("signed char overflow:  %d + 1 ", overflow++);
   printf("becomes %d\n", overflow);

   signed char underflow = SCHAR_MIN;
   printf("signed char underflow:  %d - 1 ", underflow--);
   printf("becomes %d\n", underflow);
}


///////////////////////////////////////////////////////////////////////////////
/// unsigned char

/// Print the characteristics of the "unsigned char" datatype
void doUnsignedChar() {
   printf(TABLE_FORMAT_CHAR, "unsigned char", CHAR_BIT, CHAR_BIT/8, 0, UCHAR_MAX);  // See the comment next to UCHAR_MAX in limits.h
}

/// Print the overflow/underflow characteristics of the "unsigned char" datatype
void flowUnsignedChar() {
   unsigned char overflow = UCHAR_MAX;
   printf("unsigned char overflow:  %d + 1 ", overflow++);
   printf("becomes %d\n", overflow);

   unsigned char underflow = 0;  // Note:  See limits.h UCHAR_MAX... there's no UCHAR_MIN, it's 0.
   printf("unsigned char underflow:  %d - 1 ", underflow--);
   printf("becomes %d\n", underflow);
}

