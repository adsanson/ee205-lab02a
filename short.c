///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205 - Object Oriented Programming
/// Lab 02a - Datatypes
///
/// @file short.c
/// @version 1.0
///
/// Print the characteristics of the "short", "signed short" and "unsigned short" datatypes.
///
/// @author Albert D'Sanson <adsanson@hawaii.edu>
/// @brief  Lab 02 - Datatypes - EE 205 - Spr 2021
/// @date   25_01_2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <limits.h>

#include "datatypes.h"
#include "short.h"


///////////////////////////////////////////////////////////////////////////////
/// short

/// Print the characteristics of the "short" datatype
void doShort() {
printf(TABLE_FORMAT_SHORT, "short", CHAR_BIT, CHAR_BIT/8, SHRT_MIN, SHRT_MAX);
}


/// Print the overflow/underflow characteristics of the "short" datatype
void flowShort() {
   short overflow = SHRT_MAX;
   printf("short overflow:  %d + 1 ", overflow++);
   printf("becomes %d\n", overflow);

   short underflow = SHRT_MIN;
   printf("short underflow:  %d - 1 ", underflow--);
   printf("becomes %d\n", underflow);
}


///////////////////////////////////////////////////////////////////////////////
/// unsigned short

/// Print the characteristics of the "unsigned short" datatype
void doUnsignedShort() {
printf(TABLE_FORMAT_SHORT, "unsigned short", CHAR_BIT, CHAR_BIT/8, 0, USHRT_MAX);
}

/// Print the overflow/underflow characteristics of the "unsigned short" datatype
void flowUnsignedShort() {
   short overflow = USHRT_MAX;
   printf("unsigned short overflow:  %d + 1 ", overflow++);
   printf("becomes %d\n", overflow);

   short underflow = 0;
   printf("unsigned short underflow:  %d - 1 ", underflow--);
   printf("becomes %d\n", underflow);
}
