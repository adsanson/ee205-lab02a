#include <stdio.h>
#include <limits.h>

#include "datatypes.h"
#include "long.h"

void doLong() {
printf(TABLE_FORMAT_LONG, "long", CHAR_BIT, CHAR_BIT/8, LONG_MIN, LONG_MAX);
}


void flowLong() {
   long overflow = LONG_MAX;
   printf("long overflow:  %d + 1 ", overflow++);
   printf("becomes %d\n", overflow);

   long underflow = LONG_MIN;
   printf("long underflow:  %d - 1 ", underflow--);
   printf("becomes %d\n", underflow);
}


void doUnsignedLong() {
printf(TABLE_FORMAT_ULONG, "unsigned long", CHAR_BIT, CHAR_BIT/8, 0, ULONG_MAX);
}

void flowUnsignedLong() {
   long overflow = ULONG_MAX;
   printf("unsigned long overflow:  %d + 1 ", overflow++);
   printf("becomes %d\n", overflow);

   long underflow = 0;
   printf("unsigned long underflow:  %d - 1 ", underflow--);
   printf("becomes %d\n", underflow);
}
