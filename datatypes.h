///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205 - Object Oriented Programming
/// Lab 02a - Datatypes
///
/// @file datatypes.h
/// @version 1.0
///
/// @author Albert D'Sanson <adsanson@hawaii.edu>
/// @brief  Lab 02 - Datatypes - EE 205 - Spr 2021
/// @date   25_01_2021
///////////////////////////////////////////////////////////////////////////////

#define TABLE_HEADER1 "Datatype       bits bytes              Minimum              Maximum\n"
#define TABLE_HEADER2 "-------------- ---- ----- -------------------- --------------------\n"
#define TABLE_FORMAT_CHAR  "%-14s %4ld %5ld %20d %20d\n"
#define TABLE_FORMAT_SHORT "%-14s %4ld %5ld %20d %20d\n"
#define TABLE_FORMAT_INT   "%-14s %4ld %5ld %20d %20d\n"
#define TABLE_FORMAT_UINT  "%-14s %4ld %5ld %20u %20u\n"
#define TABLE_FORMAT_LONG  "%-14s %4ld %5ld %20ld %20ld\n"
#define TABLE_FORMAT_ULONG "%-14s %4ld %5ld %20lu %20lu\n"

