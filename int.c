#include <stdio.h>
#include <limits.h>

#include "datatypes.h"
#include "int.h"

void doInt() {
printf(TABLE_FORMAT_INT, "int", CHAR_BIT, CHAR_BIT/8, INT_MIN, INT_MAX);
}

void flowInt() {
   int overflow = INT_MAX;
   printf("int overflow:  %d + 1 ", overflow++);
   printf("becomes %d\n", overflow);

   int underflow = INT_MIN;
   printf("int underflow:  %d - 1 ", underflow--);
   printf("becomes %d\n", underflow);
}


void doUnsignedInt() {
printf(TABLE_FORMAT_UINT, "unsigned int", CHAR_BIT, CHAR_BIT/8, 0, UINT_MAX);
}

void flowUnsignedInt() {
   int overflow = UINT_MAX;
   printf("unsigned int overflow:  %d + 1 ", overflow++);
   printf("becomes %d\n", overflow);

   int underflow = 0;
   printf("unsigned int underflow:  %d - 1 ", underflow--);
   printf("becomes %d\n", underflow);
}
                                               
